package mawla.golam.golammawla.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import mawla.golam.golammawla.R;
import mawla.golam.golammawla.activity.UserInfoActivity;
import mawla.golam.golammawla.model.UserInfo;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserListViewHolder> {

    Context mContext;
    List<UserInfo> userList;
    public UserListAdapter(Context context, List<UserInfo> list){
        this.mContext = context;
        this.userList = list;
    }
    @Override
    public UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(mContext);
        View rootView = inflater.inflate(R.layout.row_items_content, parent, false);
        UserListViewHolder mHolder = new UserListViewHolder(rootView);

        return mHolder;
    }

    @Override
    public void onBindViewHolder(UserListViewHolder holder, int position) {

        UserInfo mUserInfo = userList.get(position);
        holder.txtV_userName.setText(mUserInfo.name_first+" "+mUserInfo.name_last);
        holder.txtV_userCompany.setText(mUserInfo.company);
        holder.txt_Email.setText(mUserInfo.user_email);
        holder.txt_Phone.setText(mUserInfo.user_phone);

        holder.imgV_userThumb.setImageURI(Uri.parse(mUserInfo.user_thumb));

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class UserListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        SimpleDraweeView imgV_userThumb;
        TextView txtV_userName,txtV_userCompany,txt_Email,txt_Phone;
        public UserListViewHolder(View itemView) {
            super(itemView);
            imgV_userThumb = (SimpleDraweeView) itemView.findViewById(R.id.imgV_user_thumb);
            txtV_userName = (TextView) itemView.findViewById(R.id.txt_user_name);
            txtV_userCompany = (TextView) itemView.findViewById(R.id.txt_company);
            txt_Email = (TextView) itemView.findViewById(R.id.txt_email);
            txt_Phone = (TextView) itemView.findViewById(R.id.txt_phone);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent userIntent = new Intent(mContext, UserInfoActivity.class);
            userIntent.putExtra("user_info",userList.get(getLayoutPosition()));
            mContext.startActivity(userIntent);
        }
    }
}
