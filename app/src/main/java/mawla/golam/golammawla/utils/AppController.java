package mawla.golam.golammawla.utils;

import android.app.Application;
import android.app.ProgressDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.drawee.backends.pipeline.Fresco;


public class AppController extends Application {

    private static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    private RequestQueue mRequestQueue;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Fresco.initialize(this);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {

        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
    public <T> void addToRequestQueue(Request<T> req) {

        req.setTag(TAG);
        getRequestQueue().add(req);
    }
    public void cancellPendingRequest(Object tag) {
        if (mRequestQueue != null)
            getRequestQueue().cancelAll(tag);
    }

    ProgressDialog mProgressDialog;

    private ProgressDialog getProgressDialog(){
        if (mProgressDialog ==null){
            mProgressDialog = new ProgressDialog(getApplicationContext());
        }
        return mProgressDialog;
    }
    public void addToProgressDialog(String msg){
        getProgressDialog().setMessage(msg);
        getProgressDialog().show();
    }
    public void cancellProgressDialog(){
        if(mProgressDialog != null)
            getProgressDialog().dismiss();
    }
    public void showToast(String msg){

        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
