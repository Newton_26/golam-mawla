package mawla.golam.golammawla.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserInfo implements Parcelable{

    public String user_id;
    public String user_name;
    public String user_type;
    public String user_thumb;
    public String user_desc;
    public String user_email;
    public String user_phone;
    public String user_address;
    public String index;
    public String balance;
    public String age;
    public String eyeColor;
    public String company;
    public String name_first;
    public String name_last;
    public String gender;

    public UserInfo(){

    }

    protected UserInfo(Parcel in) {
        user_id = in.readString();
        user_name = in.readString();
        user_type = in.readString();
        user_thumb = in.readString();
        user_desc = in.readString();
        user_email = in.readString();
        user_phone = in.readString();
        user_address = in.readString();
        index = in.readString();
        balance = in.readString();
        age = in.readString();
        eyeColor = in.readString();
        company = in.readString();
        name_first = in.readString();
        name_last = in.readString();
        gender = in.readString();
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
        dest.writeString(user_name);
        dest.writeString(user_type);
        dest.writeString(user_thumb);
        dest.writeString(user_desc);
        dest.writeString(user_email);
        dest.writeString(user_phone);
        dest.writeString(user_address);
        dest.writeString(index);
        dest.writeString(balance);
        dest.writeString(age);
        dest.writeString(eyeColor);
        dest.writeString(company);
        dest.writeString(name_first);
        dest.writeString(name_last);
        dest.writeString(gender);
    }
}
