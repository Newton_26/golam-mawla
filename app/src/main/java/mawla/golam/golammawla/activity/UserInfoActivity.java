package mawla.golam.golammawla.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import mawla.golam.golammawla.R;
import mawla.golam.golammawla.model.UserInfo;

public class UserInfoActivity extends AppCompatActivity {

    UserInfo mUserInfo;
    TextView name,email,company,phone,address,gender,about,amount;
    SimpleDraweeView imgProfile;
    Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        mUserInfo = getIntent().getParcelableExtra("user_info");
        initViewComponent();

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        getSupportActionBar().setTitle("User Details");

        name.setText(mUserInfo.name_first + " " + mUserInfo.name_last);
        company.setText(mUserInfo.company.toUpperCase());
        email.setText(mUserInfo.user_email.toLowerCase());
        amount.setText(mUserInfo.balance);
        phone.setText(mUserInfo.user_phone);
        gender.setText(mUserInfo.gender);
        about.setText(mUserInfo.user_desc);
        address.setText(mUserInfo.user_address);

        imgProfile.setImageURI(Uri.parse(mUserInfo.user_thumb));
    }

    private void initViewComponent(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        name = (TextView) findViewById(R.id.txt_user_name);
        email = (TextView) findViewById(R.id.txt_email);
        company = (TextView) findViewById(R.id.txt_company);
        phone = (TextView) findViewById(R.id.txt_phone);
        address = (TextView) findViewById(R.id.txt_address);
        gender = (TextView) findViewById(R.id.txt_gender);
        about = (TextView) findViewById(R.id.txt_about);
        amount = (TextView) findViewById(R.id.txt_amount);
        imgProfile = (SimpleDraweeView) findViewById(R.id.imgV_user_thumb);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }
}
