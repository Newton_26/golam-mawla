package mawla.golam.golammawla.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mawla.golam.golammawla.R;
import mawla.golam.golammawla.adapter.UserListAdapter;
import mawla.golam.golammawla.model.UserInfo;
import mawla.golam.golammawla.utils.AppController;
import mawla.golam.golammawla.utils.CheckConnectivity;
import mawla.golam.golammawla.utils.DividerItemDecoration;
import mawla.golam.golammawla.utils.Urls;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    RecyclerView rclrViewContent;
    LinearLayoutManager mLayoutManager;
    List<UserInfo> userInfoList ;
    UserListAdapter mAdapter;
    ProgressDialog mDiag;

    private int lastVisibleItem,totalItemCount;
    private int visibleItem = 10;
    private boolean doMore = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rclrViewContent = (RecyclerView) findViewById(R.id.rclrv_content);

        userInfoList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        rclrViewContent.setLayoutManager(mLayoutManager);

        rclrViewContent.addItemDecoration(new DividerItemDecoration(MainActivity.this, RecyclerView.VERTICAL));
        mAdapter = new UserListAdapter(MainActivity.this,userInfoList);
        rclrViewContent.setAdapter(mAdapter);


        CheckConnectivity checkConnectivity = new CheckConnectivity(this);
        if (checkConnectivity.isConnected()) {
            showProgressDialog("loading...");
            getAllBoatList();
        }else {
            AppController.getInstance().showToast("Check your internet connection !!!");
        }

        rclrViewContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (doMore){
                    if (visibleItem+lastVisibleItem >= totalItemCount){
                        // load new data with pagination and limit
                        // add to list & inform adapter

                        // *** server side need to be designed for pagination an data limit to fetch data via web service from app

                        doMore = false;
                    }
                }
            }
        });


    }

    private void getAllBoatList(){

        JSONObject jObjParam = new JSONObject();
        try {
            jObjParam.put("access_token","SFvEcokkYVk/i8KBllBO+x9TrEBFcea03qBch+tUMFBNGPLBwQe3B+66CUoskJEt");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Urls.Root_URL,jObjParam,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("res: " + response.toString());
                        parseResponse(response);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppController.getInstance().showToast(error.getMessage());
                dismissProgressDialog();

//                parseError(error.networkResponse.data);

            }
        }){

        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest,TAG);

    }

    private void parseError(byte[] errorData) {
        JSONObject jObj;

        try {
            jObj = new JSONObject(new String(errorData));

//            AppController.getInstance().showToast(jObj.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseResponse(JSONObject response) {
        JSONObject jObj;

        try {
            jObj = (response);

            Log.i("user info list", jObj.toString());

            JSONArray jaData = jObj.getJSONArray("data");
            for (int i = 0; i<jaData.length(); i++) {

                UserInfo userInfo = new UserInfo();
                userInfo.user_id = jaData.getJSONObject(i).getString("id");
                userInfo.index = jaData.getJSONObject(i).getString("index");
                userInfo.balance = jaData.getJSONObject(i).getString("balance");
                userInfo.user_thumb = jaData.getJSONObject(i).getString("picture");
                userInfo.age = jaData.getJSONObject(i).getString("picture");
                userInfo.eyeColor = jaData.getJSONObject(i).getString("picture");
                userInfo.company = jaData.getJSONObject(i).getString("company");
                userInfo.user_email = jaData.getJSONObject(i).getString("email");
                userInfo.user_phone = jaData.getJSONObject(i).getString("phone");
                userInfo.user_address = jaData.getJSONObject(i).getString("address");
                userInfo.user_desc = jaData.getJSONObject(i).getString("about");
                userInfo.gender = jaData.getJSONObject(i).getString("gender");
                userInfo.name_first = jaData.getJSONObject(i).getJSONObject("name").getString("first");
                userInfo.name_last = jaData.getJSONObject(i).getJSONObject("name").getString("last");
                userInfoList.add(userInfo);
            }

            mAdapter.notifyDataSetChanged();
            dismissProgressDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showProgressDialog(String msg){
        mDiag = new ProgressDialog(MainActivity.this);
        mDiag.setMessage(msg);
        mDiag.show();
    }
    private void dismissProgressDialog(){
        mDiag.dismiss();
    }
}
